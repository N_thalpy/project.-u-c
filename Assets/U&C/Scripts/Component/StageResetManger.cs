﻿using Chronos;
using System;
using UnityEngine;

namespace HAJE.UnC.Component
{
    public class StageResetManger : MonoBehaviour
    {
        private bool _isRewinding;
        public bool IsRewinding
        {
            get
            {
                return _isRewinding;
            }
            set
            {
                if (value == _isRewinding)
                    return;

                if (value == true)
                {
                    foreach (GameObject obj in GameObject.FindObjectsOfType<GameObject>())
                    {
                        LocalClock clock = obj.GetComponent<LocalClock>();
                        if (clock != null)
                            clock.localTimeScale = 1f;
                    }

                    _GlobalClock.localTimeScale = -Math.Max(1, _PlayerTimeline.time / 3f);
                }
                else
                {
                    _GlobalClock.localTimeScale = 1f;
                    foreach (GameObject obj in GameObject.FindObjectsOfType<GameObject>())
                    {
                        LocalClock clock = obj.GetComponent<LocalClock>();
                        if (clock != null)
                            clock.localTimeScale = 1f;
                    }
                }

                _isRewinding = value;
            }
        }

        private GlobalClock _GlobalClock;
        private Timeline _PlayerTimeline;

        private void Awake()
        {
            _GlobalClock = GetComponent<GlobalClock>();
            _PlayerTimeline = GameObject.FindGameObjectWithTag("UnityChan").GetComponent<Timeline>();

            IsRewinding = false;
        }
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.R) && IsRewinding == false)
                IsRewinding = true;
        }
        private void FixedUpdate()
        {
            if (IsRewinding == true)
            {
                if (_PlayerTimeline.time <= 0f)
                    IsRewinding = false;
            }
        }
    }
}