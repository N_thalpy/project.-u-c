﻿using Chronos;
using System;
using UnityEngine;

namespace HAJE.UnC.Component
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Collider))]
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(Timeline))]
    public class ButtonManager : MonoBehaviour
    {
        public enum ButtonType
        {
            Push,
            Toggle,
        }
        public ButtonType Type;

        private Timeline _Timeline;
        private StageResetManger _StageResetManager;

        private bool _isActive;
        public bool IsActive
        {
            get
            {
                return _isActive;
            }
            private set
            {
                if (_isActive == value)
                    return;

                _isActive = value;
            }
        }
        private int _pressureCounter;
        private int _PressureCounter
        {
            get
            {
                return _pressureCounter;
            }
            set
            {
                if (_pressureCounter == value)
                    return;

                if (value < 0)
                    throw new Exception(String.Format("Pressure Counter 값이 0보다 작습니다. {0}", value));

                Debug.Log("Pressure Count Changed: " + value.ToString());
                if (value == 0)
                {
                    if (Type == ButtonType.Push)
                        IsActive = false;
                }
                else if (_pressureCounter == 0 && value != 0)
                {
                    IsActive = true;
                    _PressTime = Time.time;
                }

                _pressureCounter = value;
            }
        }
        private float _PressTime;


        private void Awake()
        {
            _Timeline = GetComponent<Timeline>();
            _StageResetManager = GameObject.Find("Timekeeper").GetComponent<StageResetManger>();

            _PressureCounter = 0;
            _isActive = false;
        }

        private void FixedUpdate()
        {
            if (_StageResetManager.IsRewinding == true)
                if (_Timeline.time < _PressTime)
                {
                    _pressureCounter = 0;
                    _isActive = false;
                    _PressTime = 0;
                }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (_StageResetManager.IsRewinding == false)
                if (collision.gameObject.GetComponent<ObjectAttribute>() != null)
                    if (collision.gameObject.GetComponent<ObjectAttribute>().IsHeavy == true)
                    {
                        _PressureCounter++;
                    }
        }
        private void OnCollisionExit(Collision collision)
        {
            if (_StageResetManager.IsRewinding == false)
                if (collision.gameObject.GetComponent<ObjectAttribute>() != null)
                    if (collision.gameObject.GetComponent<ObjectAttribute>().IsHeavy == true)
                    {
                        _PressureCounter--;
                    }
        }
    }
}