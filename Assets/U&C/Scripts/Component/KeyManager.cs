﻿using Chronos;
using UnityEngine;

namespace HAJE.UnC.Component
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Collider))]
    [RequireComponent(typeof(Timeline))]
    public class KeyManager : MonoBehaviour
    {
        private bool _isVisible;
        public bool IsVisible
        {
            get
            {
                return _isVisible;
            }
            set
            {
                if (value == _isVisible)
                    return;

                GetComponent<Renderer>().enabled = value;
                _UnityChanController.IsContainKey = !value;

                if (value == false)
                {
                    _TouchTime = _Timeline.time;
                }

                _isVisible = value;
            }
        }

        private float _TouchTime;
        private Timeline _Timeline;
        private StageResetManger _StageResetManager;
        private UnityChanController _UnityChanController;

        private void Awake()
        {
            _Timeline = GetComponent<Timeline>();
            _StageResetManager = GameObject.Find("Timekeeper").GetComponent<StageResetManger>();
            _UnityChanController = GameObject.FindGameObjectWithTag("UnityChan").GetComponent<UnityChanController>();

            IsVisible = true;
        }

        private void FixedUpdate()
        {
            if (_StageResetManager.IsRewinding == true)
            {
                if (_Timeline.time < _TouchTime)
                {
                    IsVisible = true;
                    _TouchTime = 0;
                }
            }
        }

        private void OnTriggerEnter(Collider col)
        {
            if (IsVisible == true)
                if (col.gameObject.tag == "UnityChan")
                {
                    IsVisible = false;
                }
        }
    }
}