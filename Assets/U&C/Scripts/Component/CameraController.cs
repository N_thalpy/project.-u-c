﻿using HAJE.UnC.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HAJE.UnC.Component
{
    [DisallowMultipleComponent]
    public class CameraController : MonoBehaviour
    {
        public float Distance;
        public float LerpFactor;
        public bool IsStaring;
        private Queue<StarePair> _StareQueue;

        private Camera _Camera;
        private GameObject _UnityChan;
        private float _xEuler;

        private void Awake()
        {
            #region 초기화
            _StareQueue = new Queue<StarePair>();
            _Camera = GetComponent<Camera>();
            _UnityChan = GameObject.FindGameObjectWithTag("UnityChan");

            IsStaring = false;

            transform.rotation = Quaternion.Euler(25, 225, 0);
            transform.position = _UnityChan.transform.position + transform.rotation * Vector3.back * Distance;
            _xEuler = 25;
            #endregion
        }
        private void Update()
        {
#if DEBUG
            if (Input.GetKeyDown(KeyCode.LeftBracket))
            {
                _xEuler -= 5;
            }
            if (Input.GetKeyDown(KeyCode.RightBracket))
            {
                _xEuler += 5;
            }
#endif
        }
        private void FixedUpdate()
        {
            #region Stare 관련 루틴
            if (IsStaring == false && _StareQueue.Count != 0)
                StartCoroutine(Stare(_StareQueue.Dequeue()));
            #endregion

#if DEBUG
            if (_UnityChan.GetComponent<UnityChanController>().IsStageClear == true && IsStaring == true)
                throw new Exception(String.Format("Camera가 Stare 상태일 때 UnityChan이 골 지점에 도착했습니다."));
#endif

            #region 클리어 카메라 모션
            if (_UnityChan.GetComponent<UnityChanController>().IsStageClear == true)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, _UnityChan.transform.rotation * Quaternion.Euler(20, 180, 0), 0.1f);
                transform.position = _UnityChan.transform.position - transform.rotation * (100 * Vector3.forward);
                _Camera.orthographicSize = Mathf.Lerp(_Camera.orthographicSize, 2.5f, 0.1f);
            }
            #endregion
            #region 그 외 평상시 카메라 모션
            else if (IsStaring == false)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(_xEuler, 225, 0), 0.1f);

                Ray r = new Ray(_UnityChan.transform.position, Vector3.down);
                RaycastHit[] hitInfos = Physics.RaycastAll(r);
                float mindist = float.PositiveInfinity;

                RaycastHit hitInfo = new RaycastHit();
                foreach (RaycastHit info in hitInfos)
                {
                    if (info.collider.gameObject.tag != "Floor")
                        continue;

                    if (info.collider == null)
                    {
                        hitInfo = info;
                        mindist = (_UnityChan.transform.position - hitInfo.point).magnitude;
                        continue;
                    }
                    else if (mindist > (_UnityChan.transform.position - info.point).magnitude)
                    {
                        mindist = (_UnityChan.transform.position - hitInfo.point).magnitude;
                        hitInfo = info;
                        continue;
                    }
                }

                Vector3 dstvec = hitInfo.collider == null ? _UnityChan.transform.position : hitInfo.point;
                dstvec += transform.rotation * Vector3.back * Distance;
                if (_UnityChan.GetComponent<UnityChanController>()._TrackedFloor != null)
                {
                    dstvec = new Vector3(dstvec.x, transform.position.y, dstvec.z);
                    transform.position = Vector3.Lerp(transform.position, dstvec, LerpFactor * Time.fixedDeltaTime);
                }
                else
                {
                    transform.position = Vector3.Lerp(transform.position, dstvec, LerpFactor * Time.fixedDeltaTime);
                }
            }
            #endregion
        }

        public void EnqueueStare(GameObject obj, float duration)
        {
            StarePair spair = new StarePair(obj, duration);
            Debug.Log(String.Format("Stare Object Enqueued; {0}", spair));
            _StareQueue.Enqueue(spair);
        }
        private IEnumerator Stare(StarePair spair)
        {
            IsStaring = true;
            float duration = spair.Duration;

            while (duration > 0f)
            {
                Vector3 dstvec = spair.Target.transform.position;
                dstvec += transform.rotation * Vector3.back * Distance;
                transform.position = Vector3.Lerp(transform.position, dstvec, LerpFactor * Time.fixedDeltaTime);

                yield return new WaitForFixedUpdate();
                duration -= Time.fixedDeltaTime;
            }

            IsStaring = false;
            yield break;
        }
    }
}