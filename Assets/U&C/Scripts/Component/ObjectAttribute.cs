﻿using Chronos;
using UnityEngine;

namespace HAJE.UnC.Component
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Timeline))]
    public class ObjectAttribute : MonoBehaviour
    {
        public bool IsGrabable;
        public bool IsControlable;
        public bool IsHeavy;
    }
}