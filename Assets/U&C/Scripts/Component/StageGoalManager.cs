﻿using System;
using UnityEngine;

namespace HAJE.UnC.Component
{
    public class StageGoalManager : MonoBehaviour
    {
        public float GoalSize;
        private void Awake()
        {
            if (GoalSize < 0f)
                throw new Exception(String.Format("Goal Size가 0보다 작습니다. {0}", GoalSize));
        }
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(transform.position, GoalSize);
        }
    }
}