﻿using Chronos;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HAJE.UnC.Component
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(Timeline))]
    public class UnityChanController : MonoBehaviour
    {
        #region public
        public float WalkSpeed;
        public float RunSpeed;
        public float RotationSpeed;

        public bool CanAccelerateObject;
        public bool CanDecelerateObject;
        public bool CanStopObject;
        #endregion
        #region private
        private List<GameObject> _ControllableList;
        private GameObject _TargetGameObject;
        private GameObject _AccelerationObject;
        private GameObject _DecelerationObject;
        private GameObject _StopObject;

        private List<GameObject> _GrabableList;
        private GameObject _GrabTargetGameObject;
        private GameObject _GrabedGameObject;
        private GameObject _StageGoal;

        private float _MaxTargetingDistance;
        private float _MaxTargetingAngle;

        private float _MovingSpeed;
        private float _RotationTimer;
        private float _moveRotation;
        private float _MoveRotation
        {
            get
            {
                return _moveRotation;
            }
            set
            {
                if (value == _moveRotation)
                {
                    _RotationTimer += Time.fixedDeltaTime;
                    if (_RotationTimer > 0.1f)
                    {
                        _RotationTimer = 0f;
                        _MotionRotation = value;
                    }

                    return;
                }

                _RotationTimer = 0f;
                _moveRotation = value;
            }
        }
        private float _MotionRotation;

        private Animator _Animator;
        private Rigidbody _Rigidbody;

        public GameObject _TrackedFloor
        {
            get;
            private set;
        }
        private StageResetManger _StageResetManager;

        private int _RunningHashCode;
        private int _WalkingHashCode;
        private int _JumpingHashCode;
        private int _StageClearHashCode;
        #endregion
        #region getter/setter
        private bool _isMoving;
        public bool IsMoving
        {
            get
            {
                return _isMoving;
            }
            private set
            {
                if (value == _isMoving)
                    return;

                if (value == true)
                {
                    if (IsRunning == true)
                    {
                        _Animator.SetBool(_RunningHashCode, true);
                    }
                    else
                    {
                        _Animator.SetBool(_WalkingHashCode, true);
                    }
                }
                else
                {
                    _Animator.SetBool(_RunningHashCode, false);
                    _Animator.SetBool(_WalkingHashCode, false);
                }

                _isMoving = value;
            }
        }
        private bool _isRunning;
        public bool IsRunning
        {
            get
            {
                return _isRunning;
            }
            private set
            {
                if (value == _isRunning)
                    return;

                if (_isMoving == true)
                {
                    _Animator.SetBool(_RunningHashCode, value);
                    _Animator.SetBool(_WalkingHashCode, value ^ true);
                }

                _isRunning = value;
            }
        }
        private bool _isJumping;
        public bool IsJumping
        {
            get
            {
                return _isJumping;
            }
            private set
            {
                if (value == _isJumping)
                    return;

                if (value == true)
                {
                    Ray r = new Ray(transform.position + Vector3.up * 10, Vector3.down);
                    foreach (RaycastHit hitInfo in Physics.RaycastAll(r, 100))
                        if (hitInfo.collider.gameObject.tag == "Floor")
                        {
                            _TrackedFloor = hitInfo.collider.gameObject;
                            break;
                        }

                    _Rigidbody.AddForce(Vector3.up * 225);
                    _Animator.SetBool(_JumpingHashCode, true);
                }
                else
                {
                    _TrackedFloor = null;
                    _Animator.SetBool(_JumpingHashCode, false);
                }

                _isJumping = value;
            }
        }
        private bool _isStageClear;
        public bool IsStageClear
        {
            get
            {
                return _isStageClear;
            }
            private set
            {
                if (value == _isStageClear)
                    return;

                if (value == true)
                {
                    IsMoving = false;
                    IsJumping = false;
                    _Animator.SetBool(_StageClearHashCode, true);
                }
                else
                {
                    throw new Exception(String.Format("IsStageClear : {0}", value));
                }

                _isStageClear = value;
            }
        }
        private bool _isContainKey;
        public bool IsContainKey
        {
            get
            {
                return _isContainKey;
            }
            set
            {
                if (value == _isContainKey)
                    return;

                _isContainKey = value;
            }
        }
        #endregion

        private void Awake()
        {
            #region Assert
            if (WalkSpeed <= 0f)
                throw new Exception(String.Format("WalkSpeed의 범위에 문제가 있습니다: {0}", WalkSpeed));
            if (RunSpeed <= 0f)
                throw new Exception(String.Format("RunSpeed의 범위에 문제가 있습니다: {0}", RunSpeed));
            if (RotationSpeed <= 0f)
                throw new Exception(String.Format("RotationSpeed의 범위에 문제가 있습니다: {0}", RotationSpeed));
            #endregion
            #region 초기화
            _Animator = GetComponent<Animator>();
            _Rigidbody = GetComponent<Rigidbody>();

            _RotationTimer = 0f;
            IsMoving = false;
            IsRunning = true;

            _RunningHashCode = Animator.StringToHash("Running");
            _WalkingHashCode = Animator.StringToHash("Walking");
            _JumpingHashCode = Animator.StringToHash("Jumping");
            _StageClearHashCode = Animator.StringToHash("StageClear");

            _MoveRotation = transform.rotation.eulerAngles.y + 135;

            _MaxTargetingAngle = 30;
            _MaxTargetingDistance = 10;
            _ControllableList = new List<GameObject>();
            _GrabableList = new List<GameObject>();
            foreach (GameObject obj in GameObject.FindObjectsOfType<GameObject>())
            {
                if (obj.GetComponent<ObjectAttribute>() != null)
                {
                    if (obj.GetComponent<ObjectAttribute>().IsControlable == true)
                        _ControllableList.Add(obj);
                    if (obj.GetComponent<ObjectAttribute>().IsGrabable == true)
                        _GrabableList.Add(obj);
                }
            }

            _StageResetManager = GameObject.Find("Timekeeper").GetComponent<StageResetManger>();
            _StageGoal = GameObject.FindWithTag("StageGoal");
            #endregion
        }

        private void Update()
        {
            #region 디버그 루틴
#if DEBUG
            do
            {
                if (Input.GetKeyDown(KeyCode.F11))
                {
                    Debug.Log("Debug Key F11 Pressed");
                    IsContainKey = true;
                }
                if (Input.GetKeyDown(KeyCode.F12))
                {
                    Debug.Log("Debug Key F12 Pressed");
                    IsStageClear = true;
                }

                // Assertion
                if (IsContainKey == false && IsStageClear == true)
                {
                    throw new Exception(String.Format("열쇠를 가지지 않은 채 스테이지를 클리어 했습니다."));
                }
                if (_Animator.GetBool(_RunningHashCode) == true && _Animator.GetBool(_WalkingHashCode) == true)
                {
                    throw new Exception(String.Format("Animator의 Running 파라미터와 Walking 파라미터가 같이 true입니다."));
                }

                float radian = transform.rotation.eulerAngles.y / 180 * Mathf.PI;
                Vector3 centerPosition = transform.position + GetComponent<CapsuleCollider>().center;
                Vector3 distvec = new Vector3(Mathf.Sin(radian), 0, Mathf.Cos(radian)) * _MaxTargetingDistance;

                Debug.DrawRay(centerPosition, Quaternion.Euler(0, _MaxTargetingAngle, 0) * distvec, Color.green);
                Debug.DrawRay(centerPosition, Quaternion.Euler(0, -_MaxTargetingAngle, 0) * distvec, Color.green);
            } while (false);
#endif
            #endregion

            #region Left Shift 체크 (달리기/걷기)
            if (_StageResetManager.IsRewinding == false)
            {
                if (Input.GetKeyDown(KeyCode.LeftShift))
                {
                    IsRunning = false;
                }
                else if (Input.GetKeyUp(KeyCode.LeftShift))
                {
                    IsRunning = true;
                }
            }
            #endregion
            #region 이동 체크
            if (_StageResetManager.IsRewinding == false)
                if (IsStageClear == false)
                    if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.RightArrow))
                    {
                        if (Input.GetKey(KeyCode.UpArrow))
                        {
                            if (Input.GetKey(KeyCode.LeftArrow))
                                _MotionRotation = -45;
                            else if (Input.GetKey(KeyCode.RightArrow))
                                _MotionRotation = 45;
                            else
                                _MotionRotation = 0;
                        }
                        else if (Input.GetKey(KeyCode.DownArrow))
                        {
                            if (Input.GetKey(KeyCode.LeftArrow))
                                _MotionRotation = -135;
                            else if (Input.GetKey(KeyCode.RightArrow))
                                _MotionRotation = 135;
                            else
                                _MotionRotation = 180;
                        }
                        else
                        {
                            if (Input.GetKey(KeyCode.LeftArrow))
                                _MotionRotation = -90;
                            else if (Input.GetKey(KeyCode.RightArrow))
                                _MotionRotation = 90;
                        }
                        IsMoving = true;
                    }
                    else if ((Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.RightArrow)) == false)
                    {
                        IsMoving = false;
                    }
            #endregion
            #region Quaternion 보간
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, _MotionRotation - 135, 0), 0.08f);
            #endregion

            #region 타임 컨트롤 타게팅
            float mindist = _MaxTargetingDistance;
            _TargetGameObject = null;
            foreach (GameObject obj in _ControllableList)
            {
                float angle = Vector3.Angle(this.transform.rotation * Vector3.forward, obj.transform.position - this.transform.position);
                if (angle < -_MaxTargetingAngle || _MaxTargetingAngle < angle)
                    continue;

                float dist = (obj.transform.position - this.transform.position).magnitude;
                if (dist < mindist)
                {
                    mindist = dist;
                    _TargetGameObject = obj;
                }
            }
            #endregion
            #region 타임 스케일 컨트롤 루틴
            if (Input.GetKeyDown(KeyCode.Z) && CanAccelerateObject)
            {
                if (_DecelerationObject == null && _TargetGameObject != null)
                {
                    if (_AccelerationObject == _TargetGameObject)
                        _AccelerationObject = null;
                    if (_StopObject == _TargetGameObject)
                        _StopObject = null;

                    _DecelerationObject = _TargetGameObject;
                    _TargetGameObject.GetComponent<LocalClock>().localTimeScale = 0.125f;
                }
                else if (_DecelerationObject != null)
                {
                    _DecelerationObject.GetComponent<LocalClock>().localTimeScale = 1f;
                    _DecelerationObject = null;
                }
            }
            else if (Input.GetKeyDown(KeyCode.X) && CanDecelerateObject)
            {
                if (_AccelerationObject == null && _TargetGameObject != null)
                {
                    if (_DecelerationObject == _TargetGameObject)
                        _DecelerationObject = null;
                    if (_StopObject == _TargetGameObject)
                        _StopObject = null;

                    _AccelerationObject = _TargetGameObject;
                    _TargetGameObject.GetComponent<LocalClock>().localTimeScale = 2f;
                }
                else if (_AccelerationObject != null)
                {
                    _AccelerationObject.GetComponent<LocalClock>().localTimeScale = 1f;
                    _AccelerationObject = null;
                }
            }
            else if (Input.GetKeyDown(KeyCode.C) && CanStopObject)
            {
                if (_StopObject == null && _TargetGameObject != null)
                {
                    if (_AccelerationObject == _TargetGameObject)
                        _AccelerationObject = null;
                    if (_DecelerationObject == _TargetGameObject)
                        _DecelerationObject = null;

                    _StopObject = _TargetGameObject;
                    _TargetGameObject.GetComponent<LocalClock>().localTimeScale = 0f;
                }
                else if (_StopObject != null)
                {
                    _StopObject.GetComponent<LocalClock>().localTimeScale = 1f;
                    _StopObject = null;
                }
            }
            #endregion

            #region 오브젝트 집기 타게팅
            _GrabTargetGameObject = null;

            Ray r = new Ray(transform.position + GetComponent<CapsuleCollider>().center, transform.rotation * Vector3.forward);
            RaycastHit hitInfo;
            Physics.Raycast(r, out hitInfo, mindist);

            if (hitInfo.collider != null)
            {
                GameObject obj = hitInfo.collider.gameObject;
                if (obj.GetComponent<ObjectAttribute>() != null)
                    if (obj.GetComponent<ObjectAttribute>().IsGrabable == true)
                    {
                        _GrabTargetGameObject = obj;
                    }
            }
            #endregion

            #region 스테이지 골 체크
            if (IsStageClear == false && IsContainKey == true)
            {
                Vector3 dist = this.transform.position - _StageGoal.transform.position;
                if (dist.magnitude < _StageGoal.GetComponent<StageGoalManager>().GoalSize)
                {
                    Debug.Log("Stage Clear!");
                    IsStageClear = true;
                }
            }
            #endregion
            
            #region IsRewinding Check
            if (_GrabedGameObject != null)
            {
                if (_StageResetManager.IsRewinding == true)
                {
                    if (_GrabedGameObject.GetComponent<Timeline>() != null)
                        _GrabedGameObject.GetComponent<Timeline>().rigidbody.isKinematic = false;
                    else if (_GrabedGameObject.GetComponent<Rigidbody>() != null)
                        _GrabedGameObject.GetComponent<Rigidbody>().isKinematic = false;

                    _GrabedGameObject.transform.parent = null;
                    _GrabedGameObject = null;
                }
            }
            #endregion

            #region Floor Tracking
            if (_TrackedFloor != null)
                if (_TrackedFloor.transform.position.y > this.transform.position.y)
                    _TrackedFloor = null;
            #endregion
        }

        private void OnDrawGizmos()
        {
            #region 디버깅용 Gizmo
#if DEBUG
            if (_TargetGameObject != null)
            {
                Gizmos.color = Color.cyan;
                Gizmos.DrawSphere(_TargetGameObject.transform.position, 1);
            }
            if (_GrabedGameObject == null)
                if (_GrabTargetGameObject != null)
                {
                    Gizmos.color = Color.magenta;
                    Gizmos.DrawSphere(_GrabTargetGameObject.transform.position, 1.2f);
                }
#endif
            #endregion
        }

        private void FixedUpdate()
        {
            #region 오브젝트 집기/놓기/던지기
            if (_GrabedGameObject != null)
                _GrabedGameObject.transform.position = this.transform.position + 2 * Vector3.up;

            if (Input.GetKeyDown(KeyCode.E))
            {
                Debug.Log("E Pressed");
                if (_GrabedGameObject == null)
                {
                    if (_GrabTargetGameObject != null)
                    {
                        // 모든 집을 수 있는 오브젝트는 Timeline을 갖고 있어야 함.
                        _GrabTargetGameObject.transform.Translate(Vector3.up * 0.3f);
                        _GrabTargetGameObject.GetComponent<Timeline>().rigidbody.isKinematic = true;

                        _GrabedGameObject = _GrabTargetGameObject;
                    }
                    else
                        Debug.Log("No Grab Target");
                }
                else
                {
                    Vector3 force = Vector3.zero;
                    if (_GrabedGameObject.tag == "Bomb")
                    {
                        _GrabedGameObject.GetComponent<BombManager>().Thrown = true;
                        force = transform.rotation * new Vector3(0, 150, 250);
                    }

                    RigidbodyTimeline3D rb = _GrabedGameObject.GetComponent<Timeline>().rigidbody;
                    rb.isKinematic = false;
                    if (force == Vector3.zero)
                    {
                        _GrabedGameObject.transform.position = this.transform.position + Vector3.up + this.transform.rotation * Vector3.forward;
                    }
                    else
                    {
                        StartCoroutine(Fuck(rb, force));
                    }

                    _GrabedGameObject = null;
                }
            }
            #endregion

            #region 이동 루틴
            if (IsMoving)
            {
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    if (Input.GetKey(KeyCode.LeftArrow))
                        _MoveRotation = -45;
                    else if (Input.GetKey(KeyCode.RightArrow))
                        _MoveRotation = 45;
                    else
                        _MoveRotation = 0;
                }
                else if (Input.GetKey(KeyCode.DownArrow))
                {
                    if (Input.GetKey(KeyCode.LeftArrow))
                        _MoveRotation = -135;
                    else if (Input.GetKey(KeyCode.RightArrow))
                        _MoveRotation = 135;
                    else
                        _MoveRotation = 180;
                }
                else
                {
                    if (Input.GetKey(KeyCode.LeftArrow))
                        _MoveRotation = -90;
                    else if (Input.GetKey(KeyCode.RightArrow))
                        _MoveRotation = 90;
                }

                Vector3 moveVector = Quaternion.Euler(0, _MoveRotation, 0) * Vector3.forward;
                moveVector = Quaternion.Euler(0, -135, 0) * moveVector;

                if (IsRunning)
                    _MovingSpeed = (_MovingSpeed + RunSpeed * 0.08f) / 1.08f;
                else
                    _MovingSpeed = (_MovingSpeed + WalkSpeed * 0.08f) / 1.08f;

                transform.position += moveVector * _MovingSpeed * Time.fixedDeltaTime;
            }
            #endregion
            #region 점프 루틴
            if (Input.GetKey(KeyCode.Space) && IsJumping == false)
            {
                IsJumping = true;
            }
            #endregion
        }
        #region Temporary code for bug
        IEnumerator Fuck(RigidbodyTimeline3D rb, Vector3 force)
        {
            yield return new WaitForSeconds(0.1f);
            rb.velocity = _Rigidbody.velocity;
            rb.AddForce(force);
        }
        #endregion

        private void OnCollisionEnter(Collision collision)
        {
            #region 바닥!
            if (collision.gameObject.tag == "Floor")
            {
                Debug.Log(String.Format("Collision with Floor, {0}", Time.time));
                IsJumping = false;
            }
            #endregion
            #region 버튼!
            if (collision.gameObject.tag == "Button")
            {
                Debug.Log(String.Format("Collision with Button, {0}", Time.time));
                IsJumping = false;
            }
            #endregion
            #region 낙사!
            if (collision.gameObject.tag == "DeathFloor")
            {
                _StageResetManager.IsRewinding = true;
            }
            #endregion
        }
    }
}