﻿using Chronos;
using UnityEngine;

namespace HAJE.UnC.Component
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(ObjectAttribute))]
    [RequireComponent(typeof(LocalClock))]
    [RequireComponent(typeof(Timeline))]
    [RequireComponent(typeof(Collider))]
    public class BombManager : MonoBehaviour
    {
        private Timeline _Timeline;

        private StageResetManger _StageResetManager;

        private bool _isExploded;
        public bool IsExploded
        {
            get
            {
                return _isExploded;
            }
            private set
            {
                if (value == _isExploded)
                    return;

                GetComponent<Renderer>().enabled = !value;
                GetComponent<Collider>().isTrigger = value;

                if (value == true)
                {
                    _ExplosionTime = _Timeline.time;
                    foreach (Collider col in Physics.OverlapSphere(transform.position, 15))
                    {
                        if (col.gameObject != this)
                            if (col.GetComponent<Rigidbody>() != null)
                            {
                                col.GetComponent<Rigidbody>().AddExplosionForce(10, this.transform.position, (col.transform.position - this.transform.position).magnitude);
                            }
                    }
                }

                _isExploded = value;
            }
        }

        private float _ExplosionTime;
        public bool Thrown;

        private void Awake()
        {
            _Timeline = GetComponent<Timeline>();
            _StageResetManager = GameObject.Find("Timekeeper").GetComponent<StageResetManger>();

            _ExplosionTime = 0f;
            Thrown = false;
            IsExploded = false;
        }

        private void FixedUpdate()
        {
            if (_StageResetManager == true)
                if (_Timeline.time < _ExplosionTime)
                {
                    _ExplosionTime = 0f;
                    Thrown = false;
                    IsExploded = false;
                }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.tag != "UnityChan")
                if (Thrown)
                {
                    IsExploded = true;
                }
        }
    }
}