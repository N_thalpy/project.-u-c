﻿using Chronos;
using System;
using UnityEngine;

namespace HAJE.UnC.Component
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(Timeline))]
    public class FloorManager : MonoBehaviour
    {
        public AnimationCurve XPositionAnimation;
        public AnimationCurve YPositionAnimation;
        public AnimationCurve ZPositionAnimation;

        public AnimationCurve XRotationAnimation;
        public AnimationCurve YRotationAnimation;
        public AnimationCurve ZRotationAnimation;

        public GameObject Button;
        private ButtonManager _ButtonManager;

        private LocalClock _LocalClock;
        private Timeline _Timeline;
        private RigidbodyTimeline3D _Rigidbody;

        private float _AnimationTime;

        private void Awake()
        {
            if (transform.tag != "Floor")
                throw new Exception(String.Format("Tag가 Floor가 아닙니다."));

            _LocalClock = GetComponent<LocalClock>();
            _Timeline = GetComponent<Timeline>();
            _Rigidbody = _Timeline.rigidbody;

            if (Button != null)
                _ButtonManager = Button.GetComponent<ButtonManager>();

            _AnimationTime = 0;
        }
        private void FixedUpdate()
        {
            if (_ButtonManager == null || _ButtonManager.IsActive == true)
            {
                if (_LocalClock != null && _LocalClock.timeScale < 0 && _Timeline.time <= 0)
                    _LocalClock.localTimeScale = 0;
                float timeScale = _LocalClock.timeScale;
                float dt = _LocalClock.fixedDeltaTime;

                if (dt == 0f)
                {
                    _Rigidbody.velocity = Vector3.zero;
                    _Rigidbody.angularVelocity = Vector3.zero;
                    return;
                }

                float XForce = (XPositionAnimation.Evaluate(_AnimationTime + dt) - XPositionAnimation.Evaluate(_AnimationTime)) / dt;
                float YForce = (YPositionAnimation.Evaluate(_AnimationTime + dt) - YPositionAnimation.Evaluate(_AnimationTime)) / dt;
                float ZForce = (ZPositionAnimation.Evaluate(_AnimationTime + dt) - ZPositionAnimation.Evaluate(_AnimationTime)) / dt;

                float XTorque = (XRotationAnimation.Evaluate(_AnimationTime + dt) - XRotationAnimation.Evaluate(_AnimationTime)) / dt * (Mathf.PI / 180);
                float YTorque = (YRotationAnimation.Evaluate(_AnimationTime + dt) - YRotationAnimation.Evaluate(_AnimationTime)) / dt * (Mathf.PI / 180);
                float ZTorque = (ZRotationAnimation.Evaluate(_AnimationTime + dt) - ZRotationAnimation.Evaluate(_AnimationTime)) / dt * (Mathf.PI / 180);

                _Rigidbody.velocity = new Vector3(XForce, YForce, ZForce) * timeScale;
                _Rigidbody.angularVelocity = new Vector3(XTorque, YTorque, ZTorque) * timeScale;
                _AnimationTime += dt;
            }
            else
            {
                _Rigidbody.velocity = Vector3.zero;
                _Rigidbody.angularVelocity = Vector3.zero;
            }
        }
    }
}