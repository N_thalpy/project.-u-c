﻿using UnityEngine;

namespace HAJE.UnC.Component
{
    [RequireComponent(typeof(MeshRenderer))]
    public class DeathFloorManager : MonoBehaviour
    {
        private void Awake()
        {
            GetComponent<MeshRenderer>().enabled = false;
        }
    }
}
