﻿using System;
using UnityEngine;

namespace HAJE.UnC.Component
{
    public class UnityChanAnimationHandler : MonoBehaviour
    {
        private Animator _Animator;

        private void Awake()
        {
            _Animator = GetComponent<Animator>();
        }

        private void AddJumpForce()
        {
            GetComponent<Rigidbody>().AddForce(Vector3.up * 150);
        }

        public void OnCallChangeFace(String str)
        {
            _Animator.CrossFade(str, 0);
        }
    }
}