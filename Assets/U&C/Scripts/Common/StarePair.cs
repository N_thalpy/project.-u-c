﻿using System;
using UnityEngine;

namespace HAJE.UnC.Common
{
    public class StarePair
    {
        public GameObject Target;
        public float Duration;

        public StarePair(GameObject target, float duration)
        {
            if (target == null)
                throw new Exception(String.Format("Target GameObject가 null입니다."));

            this.Target = target;
            this.Duration = duration;
        }
        public override string ToString()
        {
            return String.Format("Target: {0}, Time: {1}", Target.name, Duration);
        }
    }
}